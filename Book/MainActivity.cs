﻿using System;
using Android;
using Android.App;
using Android.Content;
using Android.Media;
using Android.OS;
using Android.Preferences;
using Android.Runtime;
using Android.Support.Design.Widget;
using Android.Support.V4.View;
using Android.Support.V4.Widget;
using Android.Support.V7.App;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Webkit;
using Android.Widget;
using AlertDialog = Android.App.AlertDialog;

namespace Book
{
    [Activity(Label = "", Theme = "@style/AppTheme.NoActionBar", MainLauncher = false)]
    public class MainActivity : AppCompatActivity, NavigationView.IOnNavigationItemSelectedListener
    {
        ISharedPreferences prefs = null;

        RelativeLayout rootView;
        CardView story1;
        CardView story2;
        CardView story3;
        CardView story4;
        CardView story5;
        CardView story6;
        CardView story7;
        CardView story8;
       

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            Xamarin.Essentials.Platform.Init(this, savedInstanceState);
            SetContentView(Resource.Layout.activity_main);

            prefs = PreferenceManager.GetDefaultSharedPreferences(this);

            Android.Support.V7.Widget.Toolbar toolbar = FindViewById<Android.Support.V7.Widget.Toolbar>(Resource.Id.toolbar);
            SetSupportActionBar(toolbar);

            FloatingActionButton fab = FindViewById<FloatingActionButton>(Resource.Id.fab);
            fab.Click += FabOnClick;

            fab.Hide();

            rootView = FindViewById<RelativeLayout>(Resource.Id.rootview);

            DrawerLayout drawer = FindViewById<DrawerLayout>(Resource.Id.drawer_layout);
            ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, Resource.String.navigation_drawer_open, Resource.String.navigation_drawer_close);
            drawer.AddDrawerListener(toggle);
            toggle.SyncState();

            NavigationView navigationView = FindViewById<NavigationView>(Resource.Id.nav_view);
            navigationView.SetNavigationItemSelectedListener(this);


          
            


        }

        private void openview(int v)
        {
            try
            {
                Common.MusicPlayer.stop();
                Intent intent = new Intent(this, typeof(ReadingView));
                intent.PutExtra("story", v);
                intent.AddFlags(ActivityFlags.NoHistory);
                StartActivity(intent);
            }
            catch (Exception)
            {

                return;
            }
            
        }

        private void Story1_Click(object sender, EventArgs e)
        {
            openview(1);
        }

        

        private void Story2_Click(object sender, EventArgs e)
        {
            openview(2);
        }


        private void Story3_Click(object sender, EventArgs e)
        {
            openview(3);
        }

        private void Story4_Click(object sender, EventArgs e)
        {
            openview(4);
        }

        private void Story5_Click(object sender, EventArgs e)
        {
            openview(5);
        }

        private void Story6Click(object sender, EventArgs e)
        {
            openview(6);
        }

        private void Story7_Click(object sender, EventArgs e)
        {
            openview(7);
        }

        private void Story8_Click(object sender, EventArgs e)
        {
            openview(8);
        }
  
        public override void OnBackPressed()
        {
            try
            {

                Android.App.AlertDialog.Builder dialog = new AlertDialog.Builder(this);
                AlertDialog alert = dialog.Create();
                alert.SetTitle("Exit");
                alert.SetMessage("Do You want to exit?");
                alert.SetIcon(Resource.Drawable.alert);
                alert.SetButton("OK", (c, ev) =>
                {
                    Common.MusicPlayer.stop();
                    this.Finish();
                    base.OnBackPressed();

                });
                alert.SetButton2("CANCEL", (c, ev) => { });
                alert.Show();
            }
            catch (Exception)
            {

                return;
            }
        }

        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            MenuInflater.Inflate(Resource.Menu.menu_main, menu);
            return true;
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            int id = item.ItemId;
            //if (id == Resource.Id.action_settings)
            //{
            //    return true;
            //}

        if (id == Resource.Id.action_Author)
            {
                Android.App.AlertDialog.Builder dialog = new AlertDialog.Builder(this);
                AlertDialog alert = dialog.Create();
                alert.SetTitle("කතෘ ගැන");
                alert.SetMessage("පවනි උමයංගනා..............................................................................................................");
                alert.SetIcon(Resource.Drawable.authorIcon);
                alert.SetButton("Back", (c, ev) =>
                {
                });
                alert.Show();


                return true;
            }


            if (id == Resource.Id.action_OtherBooks)
            {
                Android.App.AlertDialog.Builder dialog = new AlertDialog.Builder(this);
                AlertDialog alert = dialog.Create();
                alert.SetTitle("කතෘගේ අනෙකුත් ග්‍රන්ථ");
                alert.SetMessage("මන්දෝදරී " + "\n"+ "දහස් කැලුම්" + "\n"+ "සමුගත් සඳ" + "\n"+ "තරු සිදුරෙන් එහා"+ "\n"+ "තුරඟ මායම්"+ "\n"+ "සසර පුරුද්දට"+ "\n"+ "කිඳුරු රැජින");
                alert.SetIcon(Resource.Drawable.authorIcon);
                alert.SetButton("Back", (c, ev) =>
                {
                });
                alert.Show();


                return true;
            }

            if (id == Resource.Id.action_exit)
            {
                Android.App.AlertDialog.Builder dialog = new AlertDialog.Builder(this);
                AlertDialog alert = dialog.Create();
                alert.SetTitle("Exit");
                alert.SetMessage("Do You want to exit?");
                alert.SetIcon(Resource.Drawable.alert);
                alert.SetButton("OK", (c, ev) =>
                {

                    Common.MusicPlayer.stop();
                    this.Finish();
                    base.OnBackPressed();
                });
                alert.SetButton2("CANCEL", (c, ev) => { });
                alert.Show();

                return true;
            }

            if (id == Resource.Id.action_ClearBookMark)
            {
                prefs.Edit().PutInt("bookmark", 0).Commit();

                Snackbar.Make(rootView, "Bookmark cleared", Snackbar.LengthLong).Show();
                return true;
            }

            return base.OnOptionsItemSelected(item);
        }

        private void FabOnClick(object sender, EventArgs eventArgs)
        {
            View view = (View) sender;
            Snackbar.Make(view, "Replace with your own action", Snackbar.LengthLong)
                .SetAction("Action", (Android.Views.View.IOnClickListener)null).Show();
        }

        public bool OnNavigationItemSelected(IMenuItem item)
        {
            int id = item.ItemId;

            if (id == Resource.Id.nav_camera)
            {
                Android.Net.Uri uri = Android.Net.Uri.Parse("market://details?id=" + "lk.stechbuzz.qrreader");
                var openmarket = new Intent(Intent.ActionView, uri);
                StartActivity(openmarket);
            }

            else if (id == Resource.Id.nav_manage)
            {
                Android.Net.Uri uri = Android.Net.Uri.Parse("market://details?id=" + "lk.stechbuzz.electronic");
                var openmarket = new Intent(Intent.ActionView, uri);
                StartActivity(openmarket);
            }

            else if (id == Resource.Id.battery)
            {
                Android.Net.Uri uri = Android.Net.Uri.Parse("market://details?id=" + "lk.stechbuzz.batteryinfo");
                var openmarket = new Intent(Intent.ActionView, uri);
                StartActivity(openmarket);
            }

            else if (id == Resource.Id.pirith)
            {
                Android.Net.Uri uri = Android.Net.Uri.Parse("market://details?id=" + "lk.stechbuzz.pirith");
                var openmarket = new Intent(Intent.ActionView, uri);
                StartActivity(openmarket);
            }

            else if (id == Resource.Id.nav_share)
            {
                Intent sendIntent = new Intent();
                sendIntent.SetAction(Intent.ActionSend);
                sendIntent.PutExtra(Intent.ExtraText, "https://play.google.com/store/apps/details?id=" + "Application.Context.PackageName");
                sendIntent.SetType("text/plain");
                StartActivity(sendIntent);
            }
            else if (id == Resource.Id.nav_send)
            {
                var emailIntent = new Intent(Android.Content.Intent.ActionSend);
                emailIntent.PutExtra(Android.Content.Intent.ExtraEmail, new[] { "stechbuzz@gmail.com" });
                emailIntent.SetType("text/plain");
                StartActivity(Intent.CreateChooser(emailIntent, "Contact with us"));
            }

            DrawerLayout drawer = FindViewById<DrawerLayout>(Resource.Id.drawer_layout);
            drawer.CloseDrawer(GravityCompat.Start);
            return true;
        }
        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Android.Content.PM.Permission[] grantResults)
        {
            Xamarin.Essentials.Platform.OnRequestPermissionsResult(requestCode, permissions, grantResults);

            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }

        protected override void OnPause()
        {
            try
            {
                Common.MusicPlayer.stop();
            }
            catch (Exception)
            {

                return;
            }
            base.OnPause();
        }


        protected override void OnDestroy()
        {
            try
            {
                Common.MusicPlayer.stop();
            }
            catch (Exception)
            {

                return;
            }
            base.OnDestroy();
        }
    }
}

