﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.Graphics.Drawables;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace Book
{
    [Activity(Label = "",Theme = "@style/MyTheme.Splash", NoHistory = true, MainLauncher = false)]
    public class seconSplashActivity : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.Second_splash);


        }

        protected override void OnResume()
        {
            base.OnResume();



            Task startupWork = new Task(() => { SimulateStartup(); });
            startupWork.Start();

        }

        


        // Prevent the back button from canceling the startup process
        public override void OnBackPressed() { }

        // Simulates background work that happens behind the splash screen
        async void SimulateStartup()
        {
            ImageView linearLayout = FindViewById<ImageView>(Resource.Id.sp_back);
            linearLayout.SetImageResource(Resource.Drawable.splash);

            await Task.Delay(5000);
            StartActivity(new Intent(Application.Context, typeof(MainActivity)));
        }
    }
}