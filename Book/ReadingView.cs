﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.Animation;
using Android.App;
using Android.Content;
using Android.Gms.Ads;
using Android.Gms.Common;
using Android.Graphics;
using Android.OS;
using Android.Preferences;
using Android.Runtime;
using Android.Support.Design.Widget;
using Android.Support.V4.Widget;
using Android.Support.V7.App;
using Android.Views;
using Android.Webkit;
using Android.Widget;
using Firebase.Analytics;
using Firebase.Iid;
using Firebase.Messaging;
using Xamarin.Essentials;
using AlertDialog = Android.App.AlertDialog;

namespace Book
{
    [Activity(Label = "", Theme = "@style/AppTheme.NoActionBar")]
    public class ReadingView : AppCompatActivity
    {
        ISharedPreferences prefs = null;
        WebView textContent;
        ImageButton Prev;
        ImageButton Next;
        int scrollHeight = 50;
        LinearLayout rootView;
        LinearLayout NavigationLayout;

        public static readonly string CHANNEL_ID = "Book3_notification_channel";
        public static readonly int NOTIFICATION_ID = 103;
        const string TAG = "MainActivity";
        FirebaseAnalytics firebaseAnalytics;

        protected AdView mAdView;


        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.webView);

            Xamarin.Essentials.Platform.Init(this, savedInstanceState);


            if (!checkInternet())
            {
                showAlert("No internet connection!");
                return;
            }

            firebaseAnalytics = FirebaseAnalytics.GetInstance(this);

            prefs = PreferenceManager.GetDefaultSharedPreferences(this);

            rootView = FindViewById<LinearLayout>(Resource.Id.rootview);
            NavigationLayout = FindViewById<LinearLayout>(Resource.Id.NavigationLayout);

            textContent = FindViewById<WebView>(Resource.Id.textContent);

            textContent.SetWebViewClient(new myWebClient(this));
            textContent.Settings.SetLayoutAlgorithm(WebSettings.LayoutAlgorithm.SingleColumn);
            textContent.Settings.JavaScriptEnabled = true;
            textContent.HorizontalScrollBarEnabled = false;
            textContent.Settings.LoadWithOverviewMode = true;
            textContent.Settings.JavaScriptCanOpenWindowsAutomatically = true;
            textContent.HapticFeedbackEnabled = false;
            textContent.Settings.DomStorageEnabled = true;
            textContent.LongClickable = false;


            int story = Intent.GetIntExtra("story", 0);
            textContent.LoadUrl("https://sites.google.com/view/jathika-thotilla/home");
            

            textContent.LongClick += TextContent_LongClick;

            int bookmarkposition = prefs.GetInt("bookmarkPosition", 0);
            int bookmarkpage = prefs.GetInt("bookmarkPage", 0);


            //if (bookmarkposition > 0 && bookmarkpage== story)
            //{
            //    Android.App.AlertDialog.Builder dialog = new AlertDialog.Builder(this);
            //    AlertDialog alert = dialog.Create();
            //    alert.SetTitle("");
            //    alert.SetMessage("You have a saved bookmark");
            //    alert.SetIcon(Resource.Drawable.alert);
            //    alert.SetButton("Go to bookmark and Continue", (c, ev) =>
            //    {
            //        textContent.ScrollTo(0, bookmarkposition);
            //    });
            //    alert.SetButton2("Start from the begining", (c, ev) => { });
            //    alert.Show();
            //}


            MobileAds.Initialize(this);

            mAdView = FindViewById<AdView>(Resource.Id.adView);
            var adRequest = new AdRequest.Builder().Build();
            mAdView.LoadAd(adRequest);


            //=============firebase Begin===============//

            if (Intent.Extras != null)
            {
                foreach (var key in Intent.Extras.KeySet())
                {
                    var value = Intent.Extras.GetString(key);
                    
                }
            }

            CreateNotificationChannel();

            IsPlayServicesAvailable();

            //login event push
            Bundle bundle = new Bundle();
            bundle.PutString("app_open", "app_open");
            firebaseAnalytics.LogEvent(FirebaseAnalytics.Event.Login, bundle);
            firebaseAnalytics.SetAnalyticsCollectionEnabled(true);

            //==========firebase End========================//



        }

        public class myWebClient : WebViewClient
        {
            public Activity mActivity;
            ProgressDialog progress;
            public myWebClient(Activity mActivity)
            {
                this.mActivity = mActivity;
            }
            public override void OnPageFinished(WebView view, string url)
            {
                progress.Hide();
                base.OnPageFinished(view, url);
            }

            public override void OnPageStarted(WebView view, string url, Bitmap favicon)
            {
                progress = new ProgressDialog(this.mActivity);
                progress.Indeterminate = true;
                progress.SetProgressStyle(Android.App.ProgressDialogStyle.Spinner);
                progress.SetMessage("Loading... Please wait...");
                progress.SetCancelable(false);
                progress.Show();
                base.OnPageStarted(view, url, favicon);
            }
        }

        private void Next_Click(object sender, EventArgs e)
        {
            int currentPosition = textContent.ScrollY;
            ObjectAnimator animator = ObjectAnimator.OfInt(textContent, "ScrollY", (currentPosition + (textContent.Height - 10)));
            animator.SetDuration(2500);
            animator.Start();
        }

        private void Prev_Click(object sender, EventArgs e)
        {
            if (textContent.ScrollY == 0)
            {
                return;
            }
            else if (textContent.ScrollY > 0)
            {
                int goPosition = (textContent.ScrollY - (textContent.Height - 10));
                if (goPosition < 0)
                {
                    goPosition = 0;
                }
                //textContent.ScrollTo(0, goPosition);
                ObjectAnimator animator = ObjectAnimator.OfInt(textContent, "ScrollY", goPosition);
                animator.SetDuration(2500);
                animator.Start();
            }
        }

        private void TextContent_LongClick(object sender, View.LongClickEventArgs e)
        {
            e.Handled = true;
        }

        public override void OnBackPressed()
        {
            try
            {
                if (textContent.CanGoBack())
                {
                    textContent.GoBack();
                }
                else
                {

                }

                //prefs.Edit().PutInt("bookmarkPosition", textContent.ScrollY).Commit();
                //prefs.Edit().PutInt("bookmarkPage", Intent.GetIntExtra("story", 0)).Commit();

                this.Finish();



            }
            catch (Exception)
            {

                return;
            }
        }

        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            MenuInflater.Inflate(Resource.Menu.story_menu, menu);
            return true;
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            int id = item.ItemId;
            //if (id == Resource.Id.action_settings)
            //{
            //    return true;
            //}
            if (id == Resource.Id.action_bookmark)
            {
                prefs.Edit().PutInt("bookmarkPosition", textContent.ScrollY).Commit();
                prefs.Edit().PutInt("bookmarkPage", Intent.GetIntExtra("story", 0)).Commit();

                Snackbar.Make(rootView, "Bookmark saved..", Snackbar.LengthLong).Show();
                return true;
            }

            if (id == Resource.Id.action_exit)
            {
                Android.App.AlertDialog.Builder dialog = new AlertDialog.Builder(this);
                AlertDialog alert = dialog.Create();
                alert.SetMessage("Do You want to exit?");
                alert.SetButton("OK", (c, ev) =>
                {
                    this.Finish();
                    base.OnBackPressed();
                });
                alert.SetButton2("CANCEL", (c, ev) => { });
                alert.Show();

                return true;
            }


            if (id == Resource.Id.action_goToTop)
            {
                textContent.ScrollTo(0, 0);
                return true;
            }

            if (id == Resource.Id.action_goToBookmark)
            {
                textContent.ScrollTo(0, prefs.GetInt("bookmarkPosition", 0));
                return true;
            }

            if (id == Resource.Id.action_ClearBookMark)
            {
                prefs.Edit().PutInt("bookmarkPosition", 0).Commit();
                prefs.Edit().PutInt("bookmarkPage", 0).Commit();

                Snackbar.Make(rootView, "Bookmark cleared", Snackbar.LengthLong).Show();
                return true;
            }

            if (id == Resource.Id.action_FullScreen || id==Resource.Id.action_fullScreenMain)
            {
                if (NavigationLayout.Visibility == ViewStates.Visible)
                {
                    SupportActionBar.Hide();
                    NavigationLayout.Visibility = ViewStates.Gone;
                }

                return true;
            }

            //if (id == Resource.Id.action_exitFullScreen)
            //{
            //    if (NavigationLayout.Visibility == ViewStates.Gone)
            //    {
            //        NavigationLayout.Visibility = ViewStates.Visible;
            //    }
            //    return true;
            //}


            return base.OnOptionsItemSelected(item);
        }

        private void FabOnClick(object sender, EventArgs eventArgs)
        {
            View view = (View)sender;
            Snackbar.Make(view, "Replace with your own action", Snackbar.LengthLong)
                .SetAction("Action", (Android.Views.View.IOnClickListener)null).Show();
        }

        private bool checkInternet()
        {
            try
            {
                var current = Connectivity.NetworkAccess;

                if (current == NetworkAccess.Internet)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {

                return false;
            }

        }


        private void showAlert(string alertmsg)
        {
            try
            {
                Android.App.AlertDialog.Builder dialog = new AlertDialog.Builder(this);
                AlertDialog alert = dialog.Create();
                alert.SetTitle("");
                alert.SetMessage(alertmsg);
                alert.SetButton("Exit", (c, ev) =>
                {
                    Finish();
                });
                alert.Show();
            }
            catch (Exception)
            {

                return;
            }

        }

        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Android.Content.PM.Permission[] grantResults)
        {
            Xamarin.Essentials.Platform.OnRequestPermissionsResult(requestCode, permissions, grantResults);

            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }



        protected override void OnPause()
        {
            try
            {
                if (mAdView != null)
                {
                    mAdView.Pause();
                }
            }
            catch (Exception)
            {

                return;
            }
            base.OnPause();
        }

        protected override void OnResume()
        {
            try
            {
                if (mAdView != null)
                {
                    mAdView.Resume();
                }
            }
            catch (Exception)
            {

                return;
            }

            base.OnResume();
        }

        protected override void OnDestroy()
        {
            try
            {

                if (mAdView != null)
                {
                    mAdView.Destroy();
                }
            }
            catch (Exception)
            {

                return;
            }

            base.OnDestroy();
        }

        class AdListener : Android.Gms.Ads.AdListener
        {
            ReadingView that;

            public AdListener(ReadingView t)
            {
                that = t;
            }

        }

        class OnClickListener : Java.Lang.Object, View.IOnClickListener
        {
            ReadingView that;

            public OnClickListener(ReadingView t)
            {
                that = t;
            }

            public void OnClick(View v)
            {
                
            }
        }

        //=====firebase check google play
        public void IsPlayServicesAvailable()
        {
            var resultCode = GoogleApiAvailability.Instance.IsGooglePlayServicesAvailable(this);
            if (resultCode == ConnectionResult.Success)
            {
                FirebaseMessaging.Instance.SubscribeToTopic("book3News");
            }
            else
            {
            }

        }

        //======firebase create notification channel
        void CreateNotificationChannel()
        {
            if (Build.VERSION.SdkInt < BuildVersionCodes.O)
            {
                return;
            }

            var channel = new NotificationChannel(CHANNEL_ID, Resources.GetString(Resource.String.app_name), NotificationImportance.Default)
            {
                Description = ""
            };

            var notificationManager = (NotificationManager)GetSystemService(NotificationService);
            notificationManager.CreateNotificationChannel(channel);
        }

    }
}